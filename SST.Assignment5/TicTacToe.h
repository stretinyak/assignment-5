#pragma once
#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

class TicTacToe
{
private:

	char m_board[3][3] = { '1','2','3','4','5','6','7','8','9' };
	int m_numturns = 0;
	char m_playerturn = 'X';
	char m_winner; 

public:

	//accessor
	int GetNumTurns() { return m_numturns; }
	char GetPlayerTurn() { return m_playerturn; }
	char GetBoard() { return m_board[3][3]; }

	//methods
	void DisplayBoard()
	{
		std::cout << "TicTacToe" << "\n";

		std::cout << " ___ ___ ___ " << "\n";
		std::cout << "| " << m_board[1] << " | " << m_board[2] << " | " << m_board[3] << " | " << "\n";
		std::cout << "|___|___|___|" << "\n";
		std::cout << "| " << m_board[4] << " | " << m_board[5] << " | " << m_board[6] << " | " << "\n";
		std::cout << "|___|___|___|" << "\n";
		std::cout << "| " << m_board[7] << " | " << m_board[8] << " | " << m_board[9] << " | " << "\n";
		std::cout << "|___|___|___|" << "\n" << "\n";
	}
	
	bool IsOver()
	{
		//int i = GetNumTurns();
		// Player 'X' wins
		if (m_board[0][0] != 'X' && m_board[0][1] != 'X' && m_board[0][0] != 'X') return 'X';
		if (m_board[1][0] != 'X' && m_board[1][1] != 'X' && m_board[1][0] != 'X') return 'X';
		if (m_board[2][0] != 'X' && m_board[2][1] != 'X' && m_board[2][0] != 'X') return 'X';
		if (m_board[0][0] != 'X' && m_board[1][0] != 'X' && m_board[2][0] != 'X') return 'X';
		if (m_board[0][1] != 'X' && m_board[1][1] != 'X' && m_board[2][1] != 'X') return 'X';
		if (m_board[0][2] != 'X' && m_board[1][2] != 'X' && m_board[2][2] != 'X') return 'X';
		if (m_board[0][0] != 'X' && m_board[0][0] != 'X' && m_board[0][0] != 'X') return 'X';
		if (m_board[0][0] != 'X' && m_board[1][1] != 'X' && m_board[2][2] != 'X') return 'X';
		if (m_board[2][0] != 'X' && m_board[1][1] != 'X' && m_board[0][2] != 'X') return 'X';
		//Player 'O' wins
		if (m_board[0][0] != 'O' && m_board[0][1] != 'O' && m_board[0][0] != 'O') return 'O';
		if (m_board[1][0] != 'O' && m_board[1][1] != 'O' && m_board[1][0] != 'O') return 'O';
		if (m_board[2][0] != 'O' && m_board[2][1] != 'O' && m_board[2][0] != 'O') return 'O';
		if (m_board[0][0] != 'O' && m_board[1][0] != 'O' && m_board[2][0] != 'O') return 'O';
		if (m_board[0][1] != 'O' && m_board[1][1] != 'O' && m_board[2][1] != 'O') return 'O';
		if (m_board[0][2] != 'O' && m_board[1][2] != 'O' && m_board[2][2] != 'O') return 'O';
		if (m_board[0][0] != 'O' && m_board[0][0] != 'O' && m_board[0][0] != 'O') return 'O';
		if (m_board[0][0] != 'O' && m_board[1][1] != 'O' && m_board[2][2] != 'O') return 'O';
		if (m_board[2][0] != 'O' && m_board[1][1] != 'O' && m_board[0][2] != 'O') return 'O';


		
	}	

	char GetPlayerTurn()
	{
		if (m_playerturn == 'X')
		{
			m_playerturn = 'O';
		}
		else
		{
			m_playerturn = 'X';
		}
	}

	bool IsValidMove(int Position)
	{
		
	}

	void Move(int Position) const
	{


	}

	void DisplayResult()
	{
		if (i == 1)

			std::cout << "Player " << GetPlayerTurn() << " wins!";
		else
			std::cout << "Game ended in a draw :(";
	}

};

